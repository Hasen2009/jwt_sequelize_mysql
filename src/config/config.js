import 'dotenv/config'
module.exports = {
    development : {
        databases : {
            rest : {
                database : process.env.POSTGRESS_DB,
                username : process.env.POSTGRESS_USER,
                password : process.env.POSTGRESS_PASS,
                host : process.env.POSTGRESS_HOST,
                port : process.env.POSTGRESS_PORT,
                dialect : 'mysql',
            }
        }
    }
    }
