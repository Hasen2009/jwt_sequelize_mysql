import jwt from 'jsonwebtoken'
import 'dotenv/config'

export default { 
    verifyToken : async(req,res,next)=>{
        const bearer = req.headers.authorization;
        const bearerHeaders = bearer.split(' ')
        const accessToken = bearerHeaders[1];
        if(!accessToken) {
            return res.send({
                msg : 'invalid token'
            })
        }
        jwt.verify(accessToken,process.env.SECRETKEY,(err,data)=>{
            if(err){
                return res.send({
                    msg : 'token is expired'
                })
            }
            return res.send({
                msg : 'Successful login !!'
            })
        })
    }
}