
const sequelize = require('sequelize');
const { Sequelize } = require('sequelize');

module.exports =  (async()=>{
    const connection = new Sequelize('E4UpEPE092','E4UpEPE092','wQXQLXpUZQ',{
        host : 'remotemysql.com',
        port : 3306,
        dialect : 'mysql'
    });
    
    const Admin = connection.define('admin',{
        username : {
            type: sequelize.DataTypes.STRING
        }
    })
    
    const Token = connection.define('token',{
        token : {
            type: sequelize.DataTypes.STRING
        }
    })
    
    Admin.hasOne(Token)
    Token.belongsTo(Admin)
    await Admin.sync();
    await Token.sync();

    return {
        Admin,
        Token
    }
})()
