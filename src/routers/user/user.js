import {Router} from 'express';
import admin from '../../controllers/adminController'
import jwtToken from '../../controllers/tokenController'
import tokenMiddleware from '../../middleware/tokenMiddleware'

const router = Router();

router.post('/login', jwtToken.createToken)
router.post('/create',tokenMiddleware.verifyToken,admin.createAdmin)
router.post('/token',jwtToken.updateAccessToken)
export default router