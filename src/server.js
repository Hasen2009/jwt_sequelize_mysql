import 'dotenv/config';
import helmet from 'helmet'
import morgan from 'morgan'
import express from 'express'
import routes from './routers/index'
import db from './models'
import tokenMiddleware from './middleware/tokenMiddleware'
const app = express();
app.use(helmet());
app.use(morgan('combined'));
app.use(express.json({limit:'50mb'}));
app.use(express.urlencoded({extended:true,limit:'50mb'}));
app.use('/admin',routes.user)
// db.sequelize.sync();
app.listen(process.env.PORT);