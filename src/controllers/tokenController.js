import jwt from 'jsonwebtoken'
import 'dotenv/config'
const db = require('../models')

export default  { 
    createToken : async (req,res)=>{
        const {Token} = await db
        const {Admin} = await db
        try {
            const {username,password} = req.body;
            if(!username || !password){
                return res.send({
                    msg : 'Invalid creds'
                })
            }

            if(typeof username !=='string' || typeof password !=='string'){
                return res.send({
                    msg : 'on strings allowed'
                })
            }
            const user = {username,password}
            const adminExists = await Admin.findOne({
                where : {
                    'username' : username,
                }
            }) 
            if(!adminExists){
                return res.send({
                    msg : 'not exists'
                })
            }
            const refreshToken = jwt.sign(user,process.env.SECRETREFRESHKEY);
            const accessToken = jwt.sign(user,process.env.SECRETKEY,{expiresIn:'60s'});
            let tokenExists = await Token.findOne({
                where : {'adminId' : adminExists.id}
            })
            if(tokenExists){
                await Token.update(
                    {
                        'token' : refreshToken,
                        'adminId' : adminExists.id
                    },
                    {
                    where : {
                        'adminId' : adminExists.id
                    },

                })
            }else {
                await Token.create({
                    'token' : refreshToken,
                    'adminId' : adminExists.id
                })
            }
            res.send({
                accessToken : accessToken,
            })
        }catch(err){
            return res.send(err)
        }
    },
    updateAccessToken : async(req,res,next)=>{
        const {Token} = await db
        const {Admin} = await db
        const {refreshToken} = req.body;
        if(!refreshToken){
            return res.send({
                msg : 'invalid creds'
            })
        }
        jwt.verify(refreshToken,process.env.SECRETREFRESHKEY,async(err,data)=>{
            if(err){
                return res.send({
                    msg : 'invalid token'
                })
            }
            const user = {
                username : data.username,
                password : data.password
            }
            let adminExists = await Admin.findOne({
                where : {username : user.username}
            })
            let tokenExists = await Token.findOne({
                where : {'token' : refreshToken,'adminId' : adminExists.id}
            })

            if(tokenExists && adminExists){
                const newRefreshToken = jwt.sign(user,process.env.SECRETREFRESHKEY);
                const newAccessToken = jwt.sign(user,process.env.SECRETKEY,{expiresIn:'60s'});

                await Token.update(
                    {
                        'token' : newRefreshToken,
                    },
                    {
                    where : {
                        'adminId' : tokenExists.adminId
                    },

                })
                return res.send({
                    accessToken : newAccessToken,
                    refreshToken : newRefreshToken
                })
            }else {
                return res.send({
                    msg : 'token invalid'
                })
            }

        })
    }
}