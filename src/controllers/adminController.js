const db = require('../models/index') 
exports.createAdmin = async(req,res)=>{
    const {username , password} = req.body;
    try {
        const {Admin} = await db;
        Admin.create({
            username : username
        })
    }catch(err){
        res.send(
           err.message
        )
    }
}